//
//  SettingsViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 13/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@end
