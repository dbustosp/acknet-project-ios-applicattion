//
//  SettingsViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 13/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic)          NSArray *languages;

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.languages  = [[NSArray alloc] initWithObjects:@"English",@"Español",@"Português",nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return 3;
    
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [self.languages objectAtIndex:row];

}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    switch(row)
    {
            
        case 0:
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
                                                      forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"language"];
            NSLog(@"EN");
            break;
        case 1:
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"es", nil]
                                                      forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] setObject:@"es" forKey:@"language"];
            NSLog(@"ES");
            break;
        case 2:
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"pt", nil]
                                                      forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] setObject:@"pt" forKey:@"language"];
            NSLog(@"PT");
            break;
        default:
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
                                                      forKey:@"AppleLanguages"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
