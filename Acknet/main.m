//
//  main.m
//  Acknet
//
//  Created by Felipe Gallinari on 10/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    NSString *language = [[NSUserDefaults standardUserDefaults] stringForKey:@"language"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
                                              forKey:@"AppleLanguages"];
    if(language != nil){
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:language, nil]
                                                  forKey:@"AppleLanguages"];
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
