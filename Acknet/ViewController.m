//
//  ViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 10/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "ViewController.h"
#import "Connection.h"
#import "Helper.h"

@interface ViewController ()
@property (weak, readonly, nonatomic) IBOutlet UITextField  *txtUser;
@property (weak, readonly, nonatomic) IBOutlet UITextField  *txtPass;
@property (weak, readonly, nonatomic) IBOutlet UIButton     *btnLogin;
@property (weak, readonly, nonatomic) IBOutlet UIActivityIndicatorView *progressView;

@end

@implementation ViewController
@synthesize txtUser = _txtUser;
@synthesize txtPass = _txtPass;
@synthesize btnLogin = _btnLogin;
@synthesize progressView = _progressView;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)returnClicked:(id)sender {
    //same as login button
    [self didPressLogin];
}
- (IBAction)didPressLogin {
    [self.progressView startAnimating];
    @try {
        /* Checking for blank fields */
        if([self.txtUser.text isEqualToString:@""]){
            [Helper showMessage:@"Please enter your username." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else if([self.txtPass.text isEqualToString:@""]){
            [Helper showMessage:@"Please enter your password." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else{
            NSString* username = self.txtUser.text;
            NSString* password = self.txtPass.text;
            [self isUsername:username withPass:password];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [Helper showMessage:@"Login Failed." withTitle:@"Login Failed!"];
    }
}

- (void)isUsername:(NSString*)username withPass:(NSString*)password {
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:username, @"username", password, @"password", nil];
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *checkUser = [NSString stringWithFormat: @"%@session/", sharedConnection.IP];
        NSDictionary *result = [sharedConnection makeRequest:checkUser json:data];
        if(result != nil){
            dispatch_sync(dispatch_get_main_queue(), ^{
                BOOL success = [[result objectForKey:@"success"] intValue];
                NSString *token = [result objectForKey:@"token"];
                if(success){
                    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                    [Helper showMessage:@"And... You are in! Congratz" withTitle:@"Success!"];
                    [self.progressView stopAnimating];
                    [self performSegueWithIdentifier:@"fromLoginToMain" sender:self];
                }else{
                    [Helper showMessage:@"Wrong username or password." withTitle:@"Login Failed"];
                    [self.progressView stopAnimating];
                }
            });
        } else {
            [Helper showMessage:@"Could not connect to the server." withTitle:@"No connection"];
            [self.progressView stopAnimating];
        }
    });
        
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController *target = segue.destinationViewController;
    
    if ([segue.identifier isEqualToString:@"toMain"]) {
//        ymd_t ymd = [self selectedYMD];
//        Memory *m = [[Memory alloc] initWithId:-1 year:ymd.year month:ymd.month day:ymd.day log:@""];
//        target.memory = m;
    } else if ([segue.identifier isEqualToString:@"toEdit"]) {
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}


@end
