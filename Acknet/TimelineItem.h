//
//  TimelineItem.h
//  Acknet
//
//  Created by Felipe Gallinari on 12/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimelineItem : NSObject{
    
}
@property (nonatomic, strong, readwrite) NSString *username;
@property (nonatomic, strong, readwrite) NSString *body;
@property (nonatomic, strong, readwrite) NSString *attType;
@property (nonatomic, strong, readwrite) NSString *pDate;
@property (nonatomic, strong, readwrite) NSString *link;
@property (nonatomic, strong, readwrite) NSString *alt;
@property (nonatomic, strong, readwrite) NSString *longit;
@property (nonatomic, strong, readwrite) NSString *lat;
@property (nonatomic, strong, readwrite) NSString *typeString;
@property (nonatomic, strong, readwrite) NSString *attURL;
@property (nonatomic, strong, readwrite) NSString *itemId;
@end
