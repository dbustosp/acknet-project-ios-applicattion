//
//  TimelineItemCell.m
//  Acknet
//
//  Created by Felipe Gallinari on 03/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "TimelineItemCell.h"

@implementation TimelineItemCell
@synthesize profileImage = _profileImage;
@synthesize username = _username;
@synthesize pDate = _pDate;
@synthesize actionLabel = _actionLabel;
@synthesize body = _body;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
