//
//  MainViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 11/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "MainViewController.h"
#import "Connection.h"
#import "Helper.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UILabel *newsFeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *messagesLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventsLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *placesLabel;
@property (weak, nonatomic) IBOutlet UILabel *picturesLabel;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}   

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didPressLogout:(id)sender {Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *user = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];

    dispatch_async(q, ^{
        NSString *urlPosts = [NSString stringWithFormat: @"%@session/%@/%@", sharedConnection.IP, user, token];
        NSDictionary *result = [sharedConnection makeDELETERequest:urlPosts];
        if(result != nil){
            dispatch_sync(dispatch_get_main_queue(), ^{
                BOOL success = [[result objectForKey:@"success"] intValue];
                if(success){
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"token"];
                    
                    [Helper showMessage:@"And... You have logged out! Congratz" withTitle:@"Success!"];
                    [self performSegueWithIdentifier:@"fromLogout" sender:self];
                }else{
                    NSString *msg = [result objectForKey:@"message"];
                    [Helper showMessage:msg withTitle:@"Logout Failed (?)"];
                }
            });
        } else {
            [Helper showMessage:@"Could not connect to the server." withTitle:@"No connection"];
        }
    });
    

}

@end
