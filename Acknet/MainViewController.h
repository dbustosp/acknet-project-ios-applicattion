//
//  MainViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 11/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@end
