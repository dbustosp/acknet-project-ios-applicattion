//
//  PhotoViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 24/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "PhotoViewController.h"
#import "Connection.h"
#import "Helper.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface PhotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, atomic) ALAssetsLibrary* library;
@property (copy,   nonatomic) NSURL *movieURL;
@property (strong, nonatomic) MPMoviePlayerController *movieController;
@property (weak, nonatomic) IBOutlet UIView *superVideoView;
@property (weak, nonatomic) IBOutlet UIWebView *videoView;

@end

@implementation PhotoViewController
@synthesize imageView = _imageView;
@synthesize library = _library;
@synthesize movieURL = _movieURL;
@synthesize movieController = _movieController;
@synthesize superVideoView = _superVideoView;
@synthesize videoView = _videoView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.library = [[ALAssetsLibrary alloc] init];
}
- (void)viewDidUnload
{
    self.library = nil;
    [super viewDidUnload];
}
- (IBAction)didPressSelectPic:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}
- (IBAction)didPressAddPic:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [Helper showMessage:@"Your camera is not ready for pictures. Try to select a picture instead." withTitle:@"Sorry"];
    }
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didPressUploadPic:(id)sender {
    UIImage *photoImage = [self imageView].image;
    NSData *photoData = [NSData data];
    NSString *photoDataString = nil;
    if(photoImage){
        photoData = [Helper compressImage:photoImage];
        //UIImageJPEGRepresentation(photoImage, 0.5); //UIImagePNGRepresentation(photoImage);
        photoDataString = [photoData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:photoDataString, @"image",username, @"username", nil];
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *uploadString = [NSString stringWithFormat: @"%@upload_image", sharedConnection.IP];
        NSDictionary *result = [sharedConnection makeRequest:uploadString json:data];
        dispatch_sync(dispatch_get_main_queue(), ^{
            BOOL success = [[result objectForKey:@"success"] intValue];
            if(success){
                [Helper showMessage:@"And... It works! Congratz" withTitle:@"Uploaded with Success!"];
            }else{
                [Helper showMessage:@"Something went wrong." withTitle:@"Upload Failed"];
            }
        });
    });
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]){
        self.movieURL = info[UIImagePickerControllerMediaURL];
    } else {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
        [self.imageView setImage:image];
    
        if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
            [self.library saveImage:image toAlbum:@"Acknet" withCompletionBlock:^(NSError *error) {
                if (error!=nil) {
                    NSLog(@"Big error: %@", [error description]);
                }
            }];
        }
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)didPressTakeVideo:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)viewDidAppear:(BOOL)animated {
    
    self.movieController = [[MPMoviePlayerController alloc] init];
    
    [self.movieController setContentURL:self.movieURL];
    [self.movieController.view setFrame:self.superVideoView.bounds];
    [self.superVideoView addSubview: self.movieController.view];
    
    [self.movieController play];
    [self embedYouTube:[self getYTUrlStr:@"https://www.youtube.com/watch?v=Zq4o_Ca14aw"] frame:self.videoView.bounds];

}
- (NSString*)getYTUrlStr:(NSString*)url
{
    if (url == nil)
        return nil;
    
    NSString *retVal = [url stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
    
    NSRange pos=[retVal rangeOfString:@"version"];
    if(pos.location == NSNotFound)
    {
        retVal = [retVal stringByAppendingString:@"?version=3&hl=en_EN"];
    }
    return retVal;
}



- (void)embedYouTube:(NSString*)url frame:(CGRect)frame {
    
    NSString* embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    NSString* html = [NSString stringWithFormat:embedHTML, url, frame.size.width, frame.size.height];
    if(self.videoView == nil) {
        self.videoView = [[UIWebView alloc] initWithFrame:frame];
        self.videoView. delegate=self;
        [self.view addSubview:self.videoView];
    }
    
    
    self.videoView.mediaPlaybackAllowsAirPlay=YES;
    self.videoView.allowsInlineMediaPlayback=YES;
    [self.videoView loadHTMLString:html baseURL:nil];
    
}
@end
