//
//  TimelineItemCell
//  Acknet
//
//  Created by Felipe Gallinari on 03/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *pDate;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UITextView *body;

@end
