//
//  ShowPostViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 08/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "ShowPostViewController.h"
#import "TimelineItemCell.h"
#import "Connection.h"
#import "Helper.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface ShowPostViewController ()
@property (strong, nonatomic) MPMoviePlayerController *movieController;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *previousLoad;
@property (strong, nonatomic) NSMutableArray *comments;

@end

@implementation ShowPostViewController
@synthesize imageView = _imageView;
@synthesize nameText = _nameText;
@synthesize dateText = _dateText;
@synthesize typeText = _typeText;
@synthesize bodyText = _bodyText;
@synthesize imageViewText = _imageViewText;
@synthesize webView = _webView;
@synthesize item = _item;
@synthesize movieController = _movieController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    _nameText.text = _item.username;
    _typeText.text = _item.typeString;
    _bodyText.text = _item.body;
    [_progressView startAnimating];
    if([_item.attType isEqualToString:@"image"]){
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_item.attURL]];
        _imageView.image = [UIImage imageWithData:imageData];
    } else if([_item.attType isEqualToString:@"video"]){
        self.movieController = [[MPMoviePlayerController alloc] init];
        NSString *youTube = [NSString stringWithFormat:@"http://www.youtube.com/v/%@?version=3&hl=en_EN",_item.attURL];
        [self embedYouTube:youTube frame:self.webView.bounds];
    }
    _nameText.text = _item.username;
    [_progressView stopAnimating];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _comments = [[NSMutableArray alloc] init];
}
-(void) viewDidAppear:(BOOL)animated{
    [self handleComments];
    [self.tableView reloadData];
    [self.progressView startAnimating];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath != nil) {
        [self.tableView scrollToRowAtIndexPath: indexPath
                              atScrollPosition: UITableViewScrollPositionBottom
                                      animated: YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.comments count];
}
- (IBAction)didPressComment:(id)sender {
    UITextView *yourtextview = [[UITextView alloc] initWithFrame:CGRectMake(12, 90, 260, 100)];
    UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(250, 600, 150, 150)];
    alert.delegate=self;
    alert.title=@"Add Comment";
    alert.message=@"Write the comment you want to add to this story below:";
    [alert setValue:yourtextview forKey:@"accessoryView"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Add Comment"];
    [alert show];

}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 0)
    {
        NSString *comment = ((UITextView*)[actionSheet valueForKey:@"accessoryView"]).text;
        if([comment isEqualToString:@""]){
            [Helper showMessage:@"You can't add an empty comment." withTitle:@"Failure"];
        } else {
            [[self progressView] startAnimating];
            Connection *sharedConnection = [Connection sharedConnection];
            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
            
            dispatch_async(q, ^{
                NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
                NSString *body = comment;
                NSString *storyId = [self item].itemId;
                NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:username, @"username_posting", body, @"comment", storyId, @"key_story", nil];
                
                NSError *error;
                NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
                
                NSString *uploadString = [NSString stringWithFormat: @"%@comment", sharedConnection.IP];
                
                NSDictionary *result = [sharedConnection makeRequest:uploadString json:data];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    BOOL success = [[result objectForKey:@"success"] intValue];
                    if(success){
                        [[self progressView] stopAnimating];
                        [self.tableView reloadData];
                    }else{
                        [Helper showMessage:@"Something went wrong." withTitle:@"Comment Failed"];
                        [[self progressView] stopAnimating];
                    }
                });
            });
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TimelineItemCell *cell = (TimelineItemCell*)[tableView dequeueReusableCellWithIdentifier:@"commentCell"];
    if (!cell) {
        cell = [[TimelineItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"commentCell"];
    }
    //cell.profileImage.text = [[self.comments objectAtIndex:[indexPath row]] name];
    cell.username.text = [[self.comments objectAtIndex:[indexPath row]] username];
    cell.pDate.text = [[self.comments objectAtIndex:[indexPath row]] pDate];
    cell.body.text = [[self.comments objectAtIndex:[indexPath row]] body];
    return cell;
}
- (void)embedYouTube:(NSString*)url frame:(CGRect)frame {
    
    NSString* embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    NSString* html = [NSString stringWithFormat:embedHTML, url, frame.size.width, frame.size.height];
    if(self.webView == nil) {
        self.webView = [[UIWebView alloc] initWithFrame:frame];
        self.webView. delegate = self;
        [self.view addSubview:self.webView];
    }
    
    
    self.webView.mediaPlaybackAllowsAirPlay=YES;
    self.webView.allowsInlineMediaPlayback=YES;
    [self.webView loadHTMLString:html baseURL:nil];
    
}
- (void)handleComments {
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSString *urlPosts = [NSString stringWithFormat: @"%@comment/%@", sharedConnection.IP, [self item].itemId];
        NSDictionary *result = [sharedConnection makeGETRequest:urlPosts];
        if(result != nil){
            dispatch_sync(dispatch_get_main_queue(), ^{
                BOOL success = [[result objectForKey:@"success"] intValue];
                NSArray *commentsArray = [result objectForKey:@"comments"];
                if(success){
                    if(![commentsArray isEqualToArray:self.previousLoad]){
                        self.comments = [[NSMutableArray alloc] init];
                        self.previousLoad = commentsArray;
                        for (NSDictionary *comment in commentsArray) {
                            TimelineItem *item = [[TimelineItem alloc] init];
                            item.itemId = [comment objectForKey:@"_id"];
                            item.body = [comment objectForKey:@"body"];
                            item.username = [comment objectForKey:@"username"];
                            NSDateFormatter *df = [[NSDateFormatter alloc] init];
                            [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                            NSString *value = [[comment objectForKey:@"date"] substringWithRange:NSMakeRange(0, [[comment objectForKey:@"date"] length] - 5)];
                            NSDate *myDate = [df dateFromString: value];
                            [df setDateFormat:@"MM/dd/yyyy 'at' hh:mm a"];
                            item.pDate = [df stringFromDate:myDate];
                            [self.comments addObject:item];
                            
                            [self.tableView reloadData];
                            
                        }
                    } else {
                        [self.tableView reloadData];
                    }
                    
                    [self.progressView stopAnimating];
                }else{
                    [Helper showMessage:@"No comments to show." withTitle:@"Failure"];
                    [self.progressView stopAnimating];
                }
            });
        } else {
            [Helper showMessage:@"Could not connect to the server." withTitle:@"No connection"];
            [self.progressView stopAnimating];
        }
    });
    
}

@end
