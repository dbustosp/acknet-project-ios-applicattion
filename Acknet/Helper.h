//
//  Helper.h
//  Acknet
//
//  Created by Felipe Gallinari on 14/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject
+ (void) showMessage:(NSString*)message withTitle:(NSString*)title;
+ (NSData *)compressImage:(UIImage *)image;
+ (NSDate*) dateFromJSONString:(NSString *)dateString;
+ (NSDate *)mfDateFromDotNetJSONString:(NSString *)string;
@end
