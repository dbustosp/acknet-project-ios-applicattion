//
//  NewStoryViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 07/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewStoryViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate>

@end
