//
//  TimelineViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 26/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "TimelineViewController.h"
#import "TimelineItem.h"
#import "TimelineItemCell.h"
#import "ShowPostViewController.h"
#import "Connection.h"
#import "Helper.h"


@interface TimelineViewController ()
@property (strong, nonatomic) NSMutableArray *posts;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *previousLoad;
@end

@implementation TimelineViewController
@synthesize posts = _posts;
@synthesize progressView = _progressView;
@synthesize tableView = _tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.posts = [[NSMutableArray alloc] init];
}
- (void)viewDidAppear:(BOOL)animated{
    [self handlePosts];
    [self.tableView reloadData];
    [self.progressView startAnimating];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath != nil) {
        [self.tableView scrollToRowAtIndexPath: indexPath
                                  atScrollPosition: UITableViewScrollPositionBottom
                                          animated: YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TimelineItemCell *cell = (TimelineItemCell*)[tableView dequeueReusableCellWithIdentifier:@"onlyTextCell"];
    if (!cell) {
        cell = [[TimelineItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"onlyTextCell"];
    }
    //cell.profileImage.text = [[self.posts objectAtIndex:[indexPath row]] name];
    cell.username.text = [[self.posts objectAtIndex:[indexPath row]] username];
    cell.pDate.text = [[self.posts objectAtIndex:[indexPath row]] pDate];
    cell.actionLabel.text = [[self.posts objectAtIndex:[indexPath row]] typeString];
    cell.body.text = [[self.posts objectAtIndex:[indexPath row]] body];
    return cell;
}

- (void)handlePosts {
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        //NSDictionary *dictionary = [[NSDictionary alloc] init];
        //NSError *error;
        //NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *urlPosts = [NSString stringWithFormat: @"%@story/", sharedConnection.IP];
        NSDictionary *result = [sharedConnection makeGETRequest:urlPosts];
        if(result != nil){
            dispatch_sync(dispatch_get_main_queue(), ^{
                BOOL success = [[result objectForKey:@"success"] intValue];
                NSArray *postsArray = [result objectForKey:@"stories"];
                if(success){
                    if(![postsArray isEqualToArray:self.previousLoad]){
                        self.posts = [[NSMutableArray alloc] init];
                        self.previousLoad = postsArray;
                        for (NSDictionary *post in [postsArray reverseObjectEnumerator]) {
                            TimelineItem *item = [[TimelineItem alloc] init];
                            item.itemId = [post objectForKey:@"_id"];
                            item.body = [post objectForKey:@"body"];
                            item.username = [post objectForKey:@"username"];
                            item.attType = [[post objectForKey:@"attachment"] objectForKey:@"type"];
                            if([item.attType isEqualToString:@"text"]){
                                item.typeString = @"added a new text.";
                            }else if([item.attType isEqualToString:@"image"]){
                                item.typeString = @"shared a new image.";
                                item.attURL = [[post objectForKey:@"attachment"] objectForKey:@"url"];
                            }else if([item.attType isEqualToString:@"video"]){
                                item.typeString = @"shared a new video.";
                                item.attURL = [[post objectForKey:@"attachment"] objectForKey:@"url"];
                            } else {
                                item.typeString = @"...";
                            }
                            item.link = [[post objectForKey:@"attachment"] objectForKey:@"url"];
                            item.longit = [[post objectForKey:@"geolocation"] objectForKey:@"long"];
                            item.lat = [[post objectForKey:@"geolocation"] objectForKey:@"lat"];
                            item.alt = [[post objectForKey:@"geolocation"] objectForKey:@"alt"];
                            NSDateFormatter *df = [[NSDateFormatter alloc] init];
                            [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                            NSString *value = [[post objectForKey:@"date"] substringWithRange:NSMakeRange(0, [[post objectForKey:@"date"] length] - 5)];
                            NSDate *myDate = [df dateFromString: value];
                            [df setDateFormat:@"MM/dd/yyyy 'at' hh:mm a"];
                            item.pDate = [df stringFromDate:myDate];
                            [self.posts addObject:item];
                            
                            [self.tableView reloadData];
                            
                        }
                    } else {
                        [self.tableView reloadData];
                    }
                    
                    [self.progressView stopAnimating];
                    //[self performSegueWithIdentifier:@"fromLoginToMain" sender:self];
                }else{
                    [Helper showMessage:@"No posts to show." withTitle:@"Failure"];
                    [self.progressView stopAnimating];
                }
            });
        } else {
            [Helper showMessage:@"Could not connect to the server." withTitle:@"No connection"];
            //[self.progressView stopAnimating];
        }
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TimelineItem *item = (TimelineItem*)[self.posts objectAtIndex:[indexPath row]];
    NSString *id = @"toShowText";
    if([item.attType isEqualToString:@"image"]){
        id = @"toShowImage";
    }else if([item.attType isEqualToString:@"video"]){
        id = @"toShowVideo";
    }
    [self performSegueWithIdentifier:id sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    ShowPostViewController *target = segue.destinationViewController;
    if ([segue.identifier rangeOfString:@"toShow"].location != NSNotFound) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        target.item = (TimelineItem*)[self.posts objectAtIndex:[indexPath row]];
    }
}
@end
