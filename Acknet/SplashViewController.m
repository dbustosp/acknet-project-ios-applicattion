//
//  SplashViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 24/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "SplashViewController.h"
#import "Connection.h"
#import "Helper.h"

@interface SplashViewController ()

@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;
@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    if(token != nil){
        [self checkValidToken:token];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"token"];
        
        [self.progressView stopAnimating];
        self.loadingLabel.text = @"";
        [self performSegueWithIdentifier:@"toLogin" sender:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkValidToken:(NSString*)token {
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:token, @"token", nil];
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *checkToken = [NSString stringWithFormat: @"%@validateToken/", sharedConnection.IP];
        NSDictionary *result = [sharedConnection makeRequest:checkToken json:data];
        dispatch_sync(dispatch_get_main_queue(), ^{
            BOOL success = [[result objectForKey:@"success"] intValue];
            if(success){
                [self.progressView stopAnimating];
                self.loadingLabel.text = @"";
                [self performSegueWithIdentifier:@"toMain" sender:self];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"token"];
                
                [self.progressView stopAnimating];
                self.loadingLabel.text = @"";
                [self performSegueWithIdentifier:@"toLogin" sender:self];
            }
        });
    });
}

@end
