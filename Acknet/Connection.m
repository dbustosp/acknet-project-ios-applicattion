//
//  Connection.m
//  Acknet
//
//  Created by Felipe Gallinari on 31/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//  http://www.galloway.me.uk/tutorials/singleton-classes/

#import "Connection.h"
#import "NSData+Base64.h"

@implementation Connection

@synthesize IP = _IP;

#pragma mark Singleton Methods

+ (id)sharedConnection {
    static Connection *sharedConnection = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedConnection = [[self alloc] init];
    });
    return sharedConnection;
}

- (id)init {
    if (self = [super init]) {
        _IP = @"http://arcane-crag-4782.herokuapp.com/";
        //_IP = @"http://192.168.1.111:3000/";
        //_IP = @"http://137.28.239.213:3000/";
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (NSDictionary *)makeRequest:(NSString *)urlString json:(NSData *)json {
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:json];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
    NSString *authHeader=[NSString stringWithFormat:@"Basic %@",  [self finalAuth]];
    
    [request setValue:authHeader forHTTPHeaderField:@"Authorization"];

    NSError *error = nil;
    NSURLResponse *response = [[NSURLResponse alloc] init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *result = nil;
    if(data != nil)
        result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    return result;
}
- (NSDictionary *)makeGETRequest:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *authHeader=[NSString stringWithFormat:@"Basic %@",  [self finalAuth]];
    
    [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
    [request setValue:@"no-store, no-cache, must-revalidate" forHTTPHeaderField:@"Cache-Control"];
    [request setHTTPMethod:@"GET"];
    
    NSError *error = nil;
    NSURLResponse *response = [[NSURLResponse alloc] init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *result = nil;
    
    if(data != nil)
        result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    return result;
}
- (NSDictionary *)makeDELETERequest:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *authHeader=[NSString stringWithFormat:@"Basic %@",  [self finalAuth]];

    [request setValue:authHeader forHTTPHeaderField:@"Authorization"];

    [request setHTTPMethod:@"DELETE"];
    NSError *error = nil;
    NSURLResponse *response = [[NSURLResponse alloc] init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *result = nil;
    
    if(data != nil)
        result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    return result;
}
- (BOOL)isValidYoutubeID:(NSString *)id {
    NSString *urlString = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/videos/%@",id];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response = [[NSURLResponse alloc] init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data != nil){
        NSString *someString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        if([someString isEqual: @"Invalid id"] || [someString isEqual: @"Video not found"]){
            return NO;
        }else{
            return YES;
        }
    }
    NSLog(@"here");
    return NO;
}
- (NSString*) finalAuth {
    NSString *userName = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
    NSString *password = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    if (password == nil){
        password = @"0";
    }
    
    NSString *authString = [[NSString stringWithFormat:@"%@:%@", userName, password] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *finalAuth = [[authString dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    return finalAuth;
}
@end
