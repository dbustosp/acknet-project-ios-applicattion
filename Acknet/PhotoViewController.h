//
//  PhotoViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 24/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end
