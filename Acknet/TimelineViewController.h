//
//  TimelineViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 26/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
