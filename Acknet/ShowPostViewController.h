//
//  ShowPostViewController.h
//  Acknet
//
//  Created by Felipe Gallinari on 08/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimelineItem.h"
@interface ShowPostViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageViewText;
@property (weak, nonatomic) IBOutlet UILabel *nameText;
@property (weak, nonatomic) IBOutlet UILabel *typeText;
@property (weak, nonatomic) IBOutlet UILabel *dateText;
@property (weak, nonatomic) IBOutlet UITextView *bodyText;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong, readwrite) TimelineItem *item;
@end
