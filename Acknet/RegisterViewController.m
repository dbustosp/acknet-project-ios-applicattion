//
//  RegisterViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 26/11/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "RegisterViewController.h"
#import "Connection.h"
#import "Helper.h"

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confPassword;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;

@end

@implementation RegisterViewController
@synthesize username = _username;
@synthesize password = _password;
@synthesize confPassword = _confPassword;
@synthesize email = _email;
@synthesize progressView = _progressView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //_username.placeholder = @"Entre com o usuário";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didPressJoin:(id)sender {
    [self.progressView startAnimating];
    @try {
        /* Checking for blank fields */
        if([self.username.text isEqualToString:@""]){
            [Helper showMessage:@"Please enter your username." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else if([self.password.text isEqualToString:@""]){
            [Helper showMessage:@"Please enter your password." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else if([self.email.text isEqualToString:@""]){
            [Helper showMessage:@"Please enter your email." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else if(![self.password.text isEqualToString:self.confPassword.text]){
            [Helper showMessage:@"Your password does not match with the confirmartion." withTitle:@"Error"];
            [self.progressView stopAnimating];
        }else{
            NSString* username = self.username.text;
            NSString* password = self.password.text;
            NSString* email = self.email.text;
           [self registerUsername:username withPass:password withEmail:email];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [Helper showMessage:@"Login Failed." withTitle:@"Login Failed!"];
    }

}
- (void)registerUsername:(NSString*)username withPass:(NSString*)password withEmail:(NSString*)email{
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:username, @"username", password, @"password", email, @"email", nil];
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        NSString *checkUser = [NSString stringWithFormat: @"%@user/", sharedConnection.IP];
        NSDictionary *result = [sharedConnection makeRequest:checkUser json:data];
        if(result != nil){
            dispatch_sync(dispatch_get_main_queue(), ^{
                BOOL success = [[result objectForKey:@"success"] intValue];
                NSString *token = [result objectForKey:@"token"];
                NSString *msg = [result objectForKey:@"message"];
                if(success){
                    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                    [Helper showMessage:@"Please login to your new account." withTitle:@"Success!"];
                    [self.progressView stopAnimating];
                    [[self navigationController] popViewControllerAnimated:YES];
                }else{
                    [Helper showMessage:msg withTitle:@"Error"];
                    [self.progressView stopAnimating];
                }
            });
        } else {
            [Helper showMessage:@"Could not connect to the server." withTitle:@"No connection"];
            [self.progressView stopAnimating];
        }
    });
    
}


@end
