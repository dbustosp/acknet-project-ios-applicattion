//
//  NewStoryViewController.m
//  Acknet
//
//  Created by Felipe Gallinari on 07/12/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import "NewStoryViewController.h"
#import "Helper.h"
#import "Connection.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface NewStoryViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;
@property (weak, nonatomic) IBOutlet UITextView *textViewStory;
@property (weak, nonatomic) IBOutlet UILabel *attPath;
@property NSString* realPath;
@property NSString* videoLink;
@property (strong, atomic) ALAssetsLibrary* library;
@property UIImage *imageSource;
@property NSString* placeHolder;
@end

@implementation NewStoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.library = [[ALAssetsLibrary alloc] init];
    self.placeHolder = @"Write your story here...";
    
    
    self.textViewStory.delegate = self;
    self.textViewStory.text = self.placeHolder;
    self.textViewStory.textColor = [UIColor grayColor]; //optional

}
- (void)viewDidUnload
{
    self.library = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didPressSelectPic:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}
- (IBAction)didPressAddPic:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [Helper showMessage:@"Your camera is not ready for pictures. Try to select a picture instead." withTitle:@"Sorry"];
    }
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //self.imageView = [UIImageView init];
    self.imageSource = image;
    self.videoLink = nil;
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        [self.library saveImage:image toAlbum:@"Acknet" withCompletionBlock:^(NSError *error) {
            if (error!=nil) {
                NSLog(@"Big error: %@", [error description]);
            }
        }];
    }
    // get the ref url
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    // define the block to call when we get the asset based on the url (below)
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSString *name = [imageRep filename] == nil? @"New Picture": [imageRep filename];
        [self attPath].text = [NSString stringWithFormat:@"[PICTURE] - %@",name];
    };
    
    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)didPressSendStory:(id)sender {
    [[self progressView] startAnimating];
    UIImage *imageSource = [self imageSource];
    NSData *photoData = [NSData data];
    NSString *photoDataString = nil;
    if(imageSource){
        photoData = [Helper compressImage:imageSource];
        //UIImageJPEGRepresentation(photoImage, 0.5); //UIImagePNGRepresentation(photoImage);
        photoDataString = [photoData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
    Connection *sharedConnection = [Connection sharedConnection];
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
    
    dispatch_async(q, ^{
        NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        NSString *body = [self textViewStory].text;
        NSString *type = @"text";
        NSString *link = [self videoLink] == nil ? @"": [self videoLink];
        NSString *source = photoDataString == nil ? @"": photoDataString;
        if ([self videoLink] != nil){
            type = @"video";
        } else if(photoDataString != nil) {
            type = @"image";
        }
        NSDictionary *attachment = [[NSDictionary alloc] initWithObjectsAndKeys:source, @"source",type, @"type", link, @"link",nil];
    
        NSDictionary *geolocation = nil;
        if (YES) {
            geolocation = [[NSDictionary alloc] initWithObjectsAndKeys:@"0", @"lat", @"1", @"long", @"2", @"alt", @"Garfield", @"name", nil];

        }
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:attachment, @"attachment",username, @"username", body, @"body", geolocation, @"geolocation", nil];
        
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
        
        NSString *uploadString = [NSString stringWithFormat: @"%@story", sharedConnection.IP];
        
        NSDictionary *result = [sharedConnection makeRequest:uploadString json:data];
        dispatch_sync(dispatch_get_main_queue(), ^{
            BOOL success = [[result objectForKey:@"success"] intValue];
            if(success){
                [[self progressView] stopAnimating];
                [[self navigationController] popViewControllerAnimated:YES];
            }else{
                [Helper showMessage:@"Something went wrong." withTitle:@"Upload Failed"];
                [[self progressView] stopAnimating];
            }
        });
    });
}
- (IBAction)didPressPasteVideo:(id)sender {
    [[self progressView] startAnimating];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *clipboard = pasteboard.string;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)" options:NSRegularExpressionCaseInsensitive error:&error];
    if(clipboard != nil){
        NSRange range = NSMakeRange(0, clipboard.length);
        NSArray *matches = [regex matchesInString:clipboard options:0 range:range];
        if (matches.count>0) {
            NSTextCheckingResult *match = matches[0];
            NSRange matchRange = [match rangeAtIndex:0];
            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0L);
            Connection *sharedConnection = [Connection sharedConnection];
            //visibleText = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@", [clipboard substringWithRange:matchRange]];
            dispatch_async(q, ^{
                BOOL result = [sharedConnection isValidYoutubeID:[clipboard substringWithRange:matchRange]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Paste a Video" message:@"Pasting your youtube link.\nIf nothing shows, you didn't copy the link in your clipboard." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                if(result){
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSString *visibleText = @"";
                        visibleText = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@", [clipboard substringWithRange:matchRange]];
                        [alert addButtonWithTitle:@"Paste"];
                        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                        [alert textFieldAtIndex:0].text = visibleText;
                        [[self progressView] stopAnimating];
                        [alert textFieldAtIndex:0].enabled = NO;
                        [alert show];
                    });
                } else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        alert.message = @"Your clipboard doesn't contain a valid youtube link.";
                        [[self progressView] stopAnimating];
                        [alert show];
                    });
                }
                
            });
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Paste a Video" message:@"Your clipboard doesn't contain a valid youtube link." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
            [alert show];
            [[self progressView] stopAnimating];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Paste a Video" message:@"Your clipboard is empty, please copy a youtube video to your clipboard to use this function." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alert show];
        [[self progressView] stopAnimating];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        if(![[[alertView textFieldAtIndex:0] text] isEqual: @""]){
            [self attPath].text = [NSString stringWithFormat:@"[VIDEO] - %@",[[alertView textFieldAtIndex:0] text]];
            self.videoLink = [[alertView textFieldAtIndex:0] text];
            self.imageSource = nil;
        }
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:self.placeHolder]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = self.placeHolder;
        textView.textColor = [UIColor grayColor]; //optional
    }
    [textView resignFirstResponder];
}
@end
