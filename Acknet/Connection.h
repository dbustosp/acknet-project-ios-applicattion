//
//  Connection.h
//  Acknet
//
//  Created by Felipe Gallinari on 31/10/13.
//  Copyright (c) 2013 Muffin Tigers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Connection : NSObject{
}

@property (nonatomic, retain) NSString *IP;

+(id)sharedConnection;
- (NSDictionary *)makeRequest:(NSString *)urlString json:(NSData *)json;
- (NSDictionary *)makeGETRequest:(NSString *)urlString;
- (NSDictionary *)makeDELETERequest:(NSString *)urlString;
- (BOOL)isValidYoutubeID:(NSString *)id;
@end
